#
# Python Version: 	3.8.1
# Language:       	English
# Platform:       	Win10
# Author:         	J.Tejada <jtejada@relevantradio.com>
# Project:			Upload
#
""" Uploads show files to multiple destinations
"""
import sys
import requests
import json
from pathlib import Path
from uploadlib.audition import Audition_Checker
from uploadlib.upload_class import Upload
from uploadlib.o_drive_sync import O_Sync
from uploadlib.settings import (
    O_SHOW_PATH, O_XDS_PATH,
    SHOW_PATH, XDS_PATH,
    SERVER_DEPLOYMENT
)


def check_drives(drive_list: list):
    """ if necessary drives unavailable, prints
    statement and exits program.
    """
    for drive in drive_list:
        if not drive_exist(drive):
            print(f'{drive}: Drive is not available..')
            return False
    return True


def drive_exist(drive_letter) -> bool:
    """ checks if a drive exists
    """
    return Path(f'{drive_letter}:\\').exists()


def arg_check() -> bool:
    """ Gets passed arguments to determine execution path.
    """
    force_args = ['--force', '-f']
    argument_list = sys.argv[1:]
    for argument in argument_list:
        return (argument in force_args)


def trigger_utility_pc():
    """ Sends HTML request to utility PC to trigger upload script there.
    """
    utility_request = requests.get('http://192.168.11.171:9002/_upload')
    return {
        'status': utility_request.status_code,
        'response': json.loads(utility_request.text)
        }


def transfer(dir_lists: list, controller_class):

    class_name = controller_class.__name__

    if class_name == 'O_Sync':
        print('--------Syncing O Drive--------')
    if class_name == 'Upload':
        print('-----------Uploading-----------')

    for each_list in dir_lists:
        one_list = Upload.clean_list_dir(each_list)
        sync_obj = controller_class(one_list)
        sync_obj.upload_all()

    if class_name == 'O_Sync':
        print('----Syncing O Drive Finished----')
    if class_name == 'Upload':
        print('-------Uploading Finished-------')


def main():

    # TO DO: Make FTP upload available even if S and O drives
    # are not available

    # Make sure drives are available
    if not check_drives(['S', 'O']) and SERVER_DEPLOYMENT:
        return 'Necessary Drives not Available'

    O_exist = Path("O:\\").exists()

    # Make sure Adobe Audition is closed to prevent corruption
    if not Audition_Checker().check():
        return 'Audition Open on Processing Machine'

    # O Drive Sync
    if not SERVER_DEPLOYMENT and O_exist:
        transfer([SHOW_PATH, XDS_PATH], O_Sync)

        if not arg_check():
            print(f'Response: {trigger_utility_pc()}', '\t'*3)
            return

    #  Upload files
    if O_exist:
        transfer([O_SHOW_PATH, O_XDS_PATH], Upload)
    else:
        transfer([SHOW_PATH, XDS_PATH], Upload)

    # remove temp directory
    print(f'Cleaning Up...', '\t'*3, end='\r')
    if Upload.delete_temp():
        print(f'Cleaning Up... DONE', '\t'*3)
    else:
        return 'Temp Directory Cleanup Failed'

    return


if __name__ == '__main__':
    main()
