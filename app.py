import uvicorn
from fastapi import FastAPI
from upload import main as upload
from uploadlib.settings import SERVER_DEPLOYMENT


app = FastAPI(debug=False) if SERVER_DEPLOYMENT else FastAPI(debug=True)


@app.get('/_upload')
async def upload_endpoint():
    try:
        message = upload()
    except Exception as e:
        return {
            'success': False,
            'error': e
            }
    if message:
        return {'success': False, 'message': message}
    return {'success': True}


def main():
    if SERVER_DEPLOYMENT:
        uvicorn.run(app, host='0.0.0.0', port=9002)
    else:
        uvicorn.run(app, host='127.0.0.1', port=9002)


if __name__ == '__main__':
    main()
