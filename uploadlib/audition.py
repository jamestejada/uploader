import psutil
from uploadlib.settings import SERVER_DEPLOYMENT


class Audition_Checker():
    """ Checks whether or not Adobe Audition is open. It will not allow
    program to continue unless Adobe Audition is closed. This is to prevent
    corruption of files.
    """
    AUDITION_PROCESS_NAME = 'Adobe Audition CC.exe'
    RETRY_LIMIT = 20

    def __init__(self):
        self.process_list = self.get_process_list()

    def check(self):
        """ Checks if Audition with retry loop prompting user to close
        Adobe Audition. Returns True if Audition is CLOSED else False.
        """
        if SERVER_DEPLOYMENT:
            return self.is_closed()

        for counter in range(self.RETRY_LIMIT):
            if self.is_closed():
                return True
            if counter == (self.RETRY_LIMIT // 2):
                print('Am I a joke to you?')
            input(
                'Please Close Adobe Audition,'
                + ' then press ENTER before continuing...'
            )
            self.process_list = self.get_process_list()

        print('Really? Ok, exiting now...')
        return False

    def get_process_list(self) -> list:
        """ Gets a list of all processes active on system
        """
        processes = psutil.process_iter(['name'])
        return [process.info['name'] for process in processes]

    def is_closed(self) -> bool:
        """ Checks if Adobe Audition is close. If Audition is closed, returns True.
        """
        return (self.AUDITION_PROCESS_NAME not in self.process_list)

# End Audition_Checker Class
