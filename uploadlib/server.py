import os
import time
from ftplib import FTP, error_perm
from uploadlib.settings import (
    TIME_OUT,
    O_SHOW_PATH,
    O_XDS_PATH,
    S_DRIVE_PATH,
    SHOW_PATH,
    XDS_PATH
)
# TO DO: Maybe for efficiency, find a way to just get self.which_server instead
# of having to determine which_server in each method.


class Server:
    """ This class determines the routes of files and method of transfer
    (i.e. FTP, file copy). The get_server() method returns a connected
    server object if the transfer method is an FTP connection.
    """
    TRAFFIC = {
        'IP': os.getenv('TRAFFIC_IP'),
        'USER': os.getenv('TRAFFIC_USER'),
        'PASSWORD': os.getenv('TRAFFIC_PASSWORD')
    }
    XDS = {
        'IP': os.getenv('XDS_IP'),
        'USER': os.getenv('XDS_USER'),
        'PASSWORD': os.getenv('XDS_PASSWORD')
    }

    def __init__(self, file_name):
        self.file_name = file_name

    def get_server(self) -> object:
        """ Returns FTP server object based on file name
        """
        credentials = self.get_credentials()
        if credentials:
            return self.FTP_connect(
                credentials.get('IP'),
                credentials.get('USER'),
                credentials.get('PASSWORD')
            )
        return None

    def get_credentials(self):
        which_server = self.destination(self.file_name)
        return {
            'XDS': self.XDS,
            'TRAFFIC': self.TRAFFIC,
            'S': None
        }.get(which_server)

    def FTP_connect(self, host, username, password, n=1) -> object:
        """ Logs into FTP given in parameters and returns the server
        after successful login.
        """
        if n > TIME_OUT:
            return None

        which_server = self.destination(self.file_name)

        server = FTP(host)
        result = server.login(username, password)
        if '230' in result:
            print(f'--------{which_server} FTP CONNECTED--------', '\t'*3)
            return server
        else:
            self.FTP_connect(host, username, password, n+1)

    @staticmethod
    def get_local_path(file_name):
        """ Returns path of the source file to upload.
        """
        if O_SHOW_PATH.exists():
            return {
                'S': O_SHOW_PATH.joinpath(file_name),
                'XDS': O_XDS_PATH.joinpath(file_name),
                'TRAFFIC': O_SHOW_PATH.joinpath(file_name)
            }.get(Server.destination(file_name))
        return {
                'S': SHOW_PATH.joinpath(file_name),
                'XDS': XDS_PATH.joinpath(file_name),
                'TRAFFIC': SHOW_PATH.joinpath(file_name)
            }.get(Server.destination(file_name))

    @staticmethod
    def get_dest_dirs(file_name: str):
        """ Returns destination directory for given file name.
        """
        LA_path = '/outgoing/air/losangeles/'
        SD_path = '/outgoing/air/sandiego/'
        PHX_path = '/outgoing/air/phx/'

        destination = Server.destination(file_name)

        if destination == 'TRAFFIC':
            return {
                'EBTS.mp3': [SD_path, LA_path],
                'OCCR.mp3': [SD_path, LA_path],
                'MFT.mp3': [SD_path, LA_path],
                'CS.mp3': [SD_path],
                'PHXBRH.mp3': [PHX_path],
                'CATCON.mp3': [PHX_path]
            }.get(file_name)

        return {
            'S': S_DRIVE_PATH,
            'XDS': '/'
        }.get(destination)
    # End get_dest_dirs

    @staticmethod
    def destination(file_name) -> str:
        """Returns destination (i.e. TRAFFIC, XDS) based on file name.
        """
        XDS_list = [
            'RREBTS.MP2', 'RREBTS.mp2',
            'RRLWOF.MP2', 'RRLWOF.mp2',
            'RRWOFL.MP2', 'RRWOFL.mp2'
            ]
        local_list = [
            "EBTS.mp3", "CATCON.mp3", "CS.mp3",
            "MFT.mp3", "OCCR.mp3", "PHXBRH.mp3"
            ]

        if file_name in local_list:
            return 'TRAFFIC'
        if file_name in XDS_list:
            return 'XDS'
        return 'S'
# End Server Class


class retry_backoff:
    """ Decorator that adds liner backoff to FTP methods/functions
    """
    INITIAL_WAIT = 30
    BACKOFF_LIMIT = 120
    WAIT_INCREMENT = 10

    def __init__(self, transfer_method: callable):
        self.wait = self.INITIAL_WAIT
        self.transfer_method = transfer_method

    def __call__(self, *args, **kwargs):
        """ Transfers file. Initiates linear backoff to retry
        on failure.
        """
        for n in self._linear_backoff():
            try:
                return self.transfer_method(*args, **kwargs)
            except error_perm as e:
                print(e)
                print(f'Trying waiting {n} seconds...')
                time.sleep(n)
        return False

    def _linear_backoff(self) -> int:
        """ Generator that yields seconds to wait.
        """
        while self.wait <= self.BACKOFF_LIMIT:
            yield self.wait
            self.wait += self.WAIT_INCREMENT
        # Reset Wait
        self.wait = self.INITIAL_WAIT
