import os
import shutil
from pathlib import Path
from uploadlib.file_upload import File_Upload
from uploadlib.settings import TEMP_DIR


class Upload:
    """ Controlling class for parsing a list of files for upload.
    """
    def __init__(self, dir_list):
        self.dir_list = dir_list

    def upload_all(self):
        """ uploads all files in self.dir_list to correct destination.
        """
        for file_name in self.dir_list:
            print(f'Checking {file_name}...', '\t'*3, end='\r')
            show_file = File_Upload(file_name)
            result = show_file.upload_and_verify()
            if show_file.should_upload and result:
                print(f'Checking {file_name}... Uploaded and Verified', '\t'*3)

    @staticmethod
    def clean_list_dir(dir_path) -> list:
        """ Returns list of mp2/mp3 files in directory and filters
        subdirectories and other files.
        """
        accepted_ext = ['.mp2', '.MP2', '.mp3', '.MP3']
        dir_lst = os.listdir(dir_path)

        return [item for item in dir_lst if Path(item).suffix in accepted_ext]

    @staticmethod
    def delete_temp() -> bool:
        """ Deletes Temporary Directory used for verification of FTP files.
        """
        try:
            if TEMP_DIR.exists():
                shutil.rmtree(TEMP_DIR)
            return True
        except Exception as e:
            print(e)
            return False
