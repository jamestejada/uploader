from uploadlib.server import Server
from uploadlib.should_upload import Should_Upload
from uploadlib.uploader import Uploader
from uploadlib.verifier import Verifier


class File_Upload:
    """ Creates an object based on a show file that allows
    upload and verification of the show file.
    """
    def __init__(self, file_name: str):

        self.file_name = file_name
        self.destination = Server.destination(self.file_name)
        self.should_upload = Should_Upload.check(self.file_name)
        self.upload = lambda: True
        self.verify = lambda: True
        self.server = None

        if self.should_upload:
            self.server = Server(self.file_name).get_server()
            class_params = (self.file_name, self.destination, self.server)
            self.upload = Uploader(*class_params).upload
            self.verify = Verifier(*class_params).verify

    def upload_and_verify(self) -> bool:
        """ Completes upload and verification all in one method.
        """
        return (self.upload() and self.verify())

    def server_quit(self) -> str:
        """ If server exists, closes server connection, returns response.
        """
        if self.server:
            return self.server.quit()

# END File_Upload Class
