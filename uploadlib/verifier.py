from hashlib import md5
from uploadlib.server import Server, retry_backoff
from uploadlib.settings import TEMP_DIR


class Verifier:
    """ Sets self.verify to a function which verifies file upload
    by comparing hashed contents. Self.verify returns a boolean.
    """
    def __init__(self, file_name: str, destination: str, server=None):

        self.destination = destination
        self.file_name = file_name
        self.server = server

        self.local_path = Server.get_local_path(self.file_name)
        self.dest_dirs = Server.get_dest_dirs(self.file_name)

        self.verify = self.verify_FTP if server else self.verify_S

    def verify_S(self) -> bool:
        """ NOTE: This method passed to Upload Class.
        Method to verify files uploaded to S: Drive
        """
        full_path = self.dest_dirs.joinpath(self.file_name)
        return self.compare_hash(full_path)

    # FTP Methods
        # Method to Pass to Upload Class
    def verify_FTP(self) -> bool:
        """ NOTE: This method passed to Upload Class.
        Method to verify files uploaded to an FTP
        """
        results = []
        for dest_dir in self.dest_dirs:
            file_path = self.file_temp_path(dest_dir)
            results.append(self.process_file(file_path, dest_dir))
        return (False not in results)

    def process_file(self, file_path, dest_dir):
        """ downloads and hashes one file to verify.
        """
        success = self.FTP_download(self, file_path, dest_dir)
        if success:
            return self.compare_hash(file_path)
        return False

    def file_temp_path(self, dest_dir):
        """ creates temp path and
        returns path object for full path to temp file
        """
        directory = self.one_temp_dir(dest_dir)
        directory.mkdir(exist_ok=True, parents=True)
        return directory.joinpath(self.file_name)

    def one_temp_dir(self, dest_dir):
        """ returns path object the temp directory depending on
        self.destination.
        """
        if self.destination == 'TRAFFIC':
            dir_list = dest_dir.split('/')
            return TEMP_DIR.joinpath(*dir_list)
        return TEMP_DIR

    @retry_backoff
    def FTP_download(self, temp_dir, dest_dir) -> bool:
        """ Downloads file to temp directory for hash verification
        """
        print(f'Downloading {dest_dir}{self.file_name}')
        with open(temp_dir, 'wb') as one_file:
            response = self.server.retrbinary(
                f'RETR {dest_dir}{self.file_name}',
                one_file.write
                )
            return ('226' in response)
        return False

    # Hash Methods
    @staticmethod
    def hash_contents(file_path) -> str:
        """ Returns an MD5 hash of file contents
        """
        with open(file_path, 'rb') as one_file:
            file_contents = one_file.read()
            return md5(file_contents).hexdigest()

    def compare_hash(self, remote_path) -> bool:
        """ Gets local file hash and remote file hash
        and returns True if hashes match.
        """
        local_hash = Verifier.hash_contents(self.local_path)
        remote_hash = Verifier.hash_contents(remote_path)

        return local_hash == remote_hash

# End Verifier Class
