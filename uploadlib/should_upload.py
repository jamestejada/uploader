import os
from datetime import timedelta
from datetime import datetime
from uploadlib.server import Server
from uploadlib.settings import S_DRIVE_PATH


class Should_Upload:
    """ This Class is a collection of static methods to help determine
    if a file needs to be uploaded.
    """
    TIME_CUTOFF = timedelta(hours=1)

    @staticmethod
    def check(file_name: str) -> bool:
        destination = Server.destination(file_name)
        local_path = Server.get_local_path(file_name)

        if destination == 'S':
            if not S_DRIVE_PATH.exists():
                return False
            remote_path = S_DRIVE_PATH.joinpath(file_name)
            return Should_Upload.compare_mod_times(local_path, remote_path)
        return Should_Upload.compare_mod_times(local_path)

    @staticmethod
    def compare_mod_times(local_path, remote_path=None) -> bool:
        """Returns True if local path is newer than or equal to
        remote path (or if local file is newer than Should_Upload.TIME_CUTOFF)
        """
        if remote_path:
            return (
                    Should_Upload.get_mod_time(remote_path)
                    < Should_Upload.get_mod_time(local_path)
            )
        return (
            (datetime.now() - Should_Upload.get_mod_time(local_path))
            < Should_Upload.TIME_CUTOFF
        )

    @staticmethod
    def get_mod_time(file_path):
        """ Returns modified time of file
        """
        return datetime.fromtimestamp(os.path.getmtime(file_path))

# End Should_Upload Class
