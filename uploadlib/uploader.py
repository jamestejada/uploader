import shutil
from uploadlib.server import Server, retry_backoff
from uploadlib.settings import S_DRIVE_PATH
# TO DO:
#   - Make Linear Backoff its own class to be used with
#       Verifier Class and File_Upload Class (both use
#       FTP operations). Maybe make it part of Server class?


class Uploader:
    """Sets self.upload to a function that will upload a file
    to the correct destination.
    """
    def __init__(self, file_name, destination, server=None):
        self.file_name = file_name
        self.destination = destination
        self.server = server

        self.local_path = Server.get_local_path(self.file_name)
        self.dest_dirs = Server.get_dest_dirs(self.file_name)

        self.upload = self.to_FTP if server else self.to_S

    # Function to Pass to Upload Class
    def to_FTP(self) -> bool:
        """ Processes list of remote FTP directories and uploads files
        to given paths.
        """
        results = []
        for dest_dir in self.dest_dirs:
            print(f'Uploading {dest_dir}{self.file_name}')
            success = self.FTP_upload(self, dest_dir)
            results.append(success)
        return (False not in results)

    @retry_backoff
    def FTP_upload(self, dest_dir) -> bool:
        """ Uploads one file to FTP self.server
        """
        if self.local_path.exists():
            with open(self.local_path, 'rb') as one_file:
                response = self.server.storbinary(
                    f'STOR {dest_dir}{self.file_name}',
                    one_file
                )
                return ('226' in response)
        return False

    # Function to Pass to Upload Class
    def to_S(self) -> bool:
        """ Uploads file to S Drive
        """
        try:
            shutil.copy(
                str(self.local_path.resolve()),
                str(S_DRIVE_PATH.joinpath(self.file_name).resolve())
            )
            return True
        except Exception as e:
            print(e)
            return False
    # End to_S

# End Uploader Class
