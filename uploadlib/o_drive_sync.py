import shutil
from pathlib import Path
from uploadlib.should_upload import Should_Upload
from uploadlib.server import Server
from uploadlib.settings import (
    SHOW_PATH,
    O_SHOW_PATH,
    XDS_PATH,
    O_XDS_PATH
)


class O_Sync:
    """ Synchronizes files from local show folders to O: Drive show folders.
    """
    # Maybe rewrite this class to be more like file_upload class.
    # and have another class that utilizes this one like upload_class.
    def __init__(self, dir_list):
        self.dir_list = dir_list

    def upload_all(self):
        """ Processes list of files in directory to determine if
        each file should be synced to O: Drive folders.
        """
        for file_name in self.dir_list:
            print(f'Checking {file_name}...     ', end='\r')
            path_tuple = self.get_tuple(file_name)
            if Path(path_tuple[1]).exists():
                if Should_Upload.compare_mod_times(*path_tuple):
                    self.copy_to_O(file_name)
            else:
                if Should_Upload.compare_mod_times(path_tuple[0]):
                    self.copy_to_O(file_name)

    def copy_to_O(self, file_name):
        """ copies from local directory to O drive directory
        """
        path_tuple = self.get_tuple(file_name)
        try:
            print(f'Copying {file_name}...          ', end='\r')
            shutil.copy(*path_tuple)
            print(f'Copying {file_name}... Success          ')
            return True
        except Exception as e:
            print(e)
            return False

    def get_tuple(self, file_name):
        """ Returns right tuple based on file_name
        """
        if Server.destination(file_name) == 'XDS':
            return self._XDS_tuple(file_name)
        return self._show_tuple(file_name)

    def _show_tuple(self, file_name):
        """ Returns tuple with strings of paths for
        shutil.copy for show upload folder. Used with
        self.get_tuple()
        """
        return (
                str(SHOW_PATH.joinpath(file_name).resolve()),
                str(O_SHOW_PATH.joinpath(file_name).resolve())
        )

    def _XDS_tuple(self, file_name):
        """ Returns tuple with strings of paths for
        shutil.copy for XDS upload folder. Used with
        self.get_tuple()
        """
        return (
                str(XDS_PATH.joinpath(file_name).resolve()),
                str(O_XDS_PATH.joinpath(file_name).resolve())
        )
