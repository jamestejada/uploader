from pathlib import Path
import os

TIME_OUT = 3

SERVER_DEPLOYMENT = (os.getenv('SERVER_DEPLOYMENT') == 'True')

USERPROFILE = os.getenv('USERPROFILE')

# Paths
S_DRIVE_PATH = Path('S:\\').joinpath('On Air mp3')
SHOW_PATH = Path(USERPROFILE).joinpath('Desktop', 'Show Uploads')
XDS_PATH = SHOW_PATH.joinpath('XDS')
O_SHOW_PATH = Path('O:\\Loomis Production\\Shared Documents\\Show Uploads')
O_XDS_PATH = O_SHOW_PATH.joinpath('XDS')
TEMP_DIR = Path.cwd().joinpath('temp')
